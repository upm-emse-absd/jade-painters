/*****************************************************************
Agent Client demanding a painting service: AgClient.java

 *****************************************************************/
package es.upm.emse.absd.agents;

import es.upm.emse.absd.ontology.painters.Estimation;
import es.upm.emse.absd.ontology.painters.EstimationRequest;
import es.upm.emse.absd.ontology.painters.PaintServOntology;
import es.upm.emse.absd.ontology.painters.Proposes;
import jade.core.Agent;
import jade.core.AID;
import jade.domain.FIPAAgentManagement.*;
import jade.domain.DFService;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import jade.content.lang.Codec;
import jade.content.lang.Codec.*;
import jade.content.lang.sl.*;

import jade.content.*;
import jade.content.onto.*;
import jade.content.onto.basic.*;

import java.util.Date;


/**
 * This agent has the following functionality: 
 * <ul>
 * <li> It register itself in the DF
 * <li> Asks the DF the name of new "painter" agents
 * <li> If there is any, it ask this agent for a service estimation;
 *      if not, waits and try it again later
 * <li> Waits for an answer to the request
 * <li> If the estimation provided is the best one among the received in a minute time,
 *      it sends an acceptation message to the painter; if not, sends one of rejection
 * </ul>
 * @author Ricardo Imbert, UPM
 * @version $Date: 2008/04/09 12:36:23 $ $Revision: 1.0 $
 **/

public class AgClient extends Agent {

	// Codec for the SL language used and instance of the ontology
	// PaintServOntology that we have created
    private final Codec codec = new SLCodec();
    private final Ontology ontology = PaintServOntology.getInstance();

    public final static String PAINTER = "Painter";

	
    protected void setup()
	{
		System.out.println(getLocalName()+": HAS ENTERED");

//      Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);

		// From this moment, it will be searching painters for a minute		
		final Date registerTime = new Date();

//		BEHAVIOURS ****************************************************************

		// Adds a behavior to search a painter agent
		// IF it is found, asks it an estimation
		// ELSE, waits 5 seconds and tries again
		addBehaviour(new SimpleBehaviour(this)
		{
			boolean end = false;
			final AID[] painters = new AID[20];
			AID ag;
			boolean ignore = false;
			int last = 0;
			int i, j;

			public void action()
			{   
				// Creates the description for the type of agent to be searched
				DFAgentDescription dfd = new DFAgentDescription();
				ServiceDescription sd = new ServiceDescription();
				sd.setType(PAINTER);
				dfd.addServices(sd);

				try
				{
					// If has been searching for estimations for more than a minute, it does not search any more
					if ((new Date()).getTime() - registerTime.getTime() >= 60000)
					{
						end = true;
					}

					// It finds agents of the required type
					DFAgentDescription[] res = DFService.search(myAgent, dfd);

					// Gets the first occurrence, if there was success
					if (res.length > 0)
					{
						for (i=0; i < res.length; i++)
						{
							ag = res[i].getName();

							for (j=0; j<last; j++)
							{
								if (painters[j].compareTo(ag) == 0)
								{
									ignore = true;
								}
							}
							// If we have not contacted yet with this painter
							if (!ignore)
							{
								painters[last++] = ag;
								// Asks the estimation to the painter
								ACLMessage msg = new ACLMessage(ACLMessage.CFP);
								msg.addReceiver(ag);
								msg.setLanguage(codec.getName());
								msg.setOntology(ontology.getName());
								EstimationRequest er = new EstimationRequest();
								// As it is an action and the encoding language the SL, it must be wrapped
								// into an Action
								Action agAction = new Action(ag,er);
								try
								{
									// The ContentManager transforms the java objects into strings
									getContentManager().fillContent(msg, agAction);
									send(msg);
									System.out.println(getLocalName()+": REQUESTS AN ESTIMATION");
								}
								catch (CodecException | OntologyException ce)
								{
									ce.printStackTrace();
								}
							}
							ignore = false;
						}
						doWait(5000);
					}
					else
					{
						// If no new PAINTER has been found, it waits 5 seconds
						doWait(5000);
					}
				}
				catch (Exception e) 
				{
					e.printStackTrace();
				}
		        }
					
		        public boolean done ()
		        {
		        	return end;
		        }
		        
			});


		// Adds a behavior to process the answer to an estimation request
		// The painter with the best estimation will be notified about its acceptation
		// while the rest will receive a reject message

		addBehaviour(new SimpleBehaviour(this)
		{
			boolean end = false;
			boolean thereisestimation = false;
			int best;
			ACLMessage replybest;

			public void action()
			{
				// Waits for the arrival of an answer
				ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.PROPOSE));
		           if (msg != null)
		            {
						try
						{
							// The ContentManager transforms the content (string) in java objects
							ContentElement ce = getContentManager().extractContent(msg);

							// We expect to find inside a Proposes predicate
							if (ce instanceof Proposes prop)
							{
								// If an estimation has arrived, it is analysed
								Estimation est = prop.getEstimation();
								int e = est.getAmount();
								System.out.println(myAgent.getLocalName()+": ESTIMATION RECEIVED"); 

								// If it is the first estimation received, it becames the best one
								if (!thereisestimation)
								{
									replybest = msg.createReply();
									best = e;
									thereisestimation = true;							
								}
								// If there was already an estimation, it checks if this new is better
								// If it is better, this becomes the best and it is notified the rejection of the previous one
								// If it is worse, it is notified directly its rejection		
								else
								{
									if (best > e)
									{
										replybest.setPerformative(ACLMessage.REJECT_PROPOSAL);
										myAgent.send(replybest);	
										System.out.println(myAgent.getLocalName()+": SENT ESTIMATION REJECTION ("+best+")");

										replybest = msg.createReply();
										best = e;
									}
									else
									{
										ACLMessage reply = msg.createReply();
										reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
										myAgent.send(reply);
										System.out.println(myAgent.getLocalName()+": SENT ESTIMATION REJECTION ("+e+")");
									}
								}
							}
							else
							{
								System.out.println(myAgent.getLocalName()+": ESTIMATION NOT UNDERSTOOD!!!");
								end = false;
							}
						}
						catch (CodecException | OntologyException cex)
						{
							cex.printStackTrace();
						}
					}
		           else
					{
						// If 60 seconds have already passed searching for estimations, the best one is accepted
						if ((new Date()).getTime() - registerTime.getTime() >= 60000)
						{
							if (thereisestimation)
							{		
								replybest.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
								myAgent.send(replybest);	
								System.out.println(myAgent.getLocalName()+": SENT ESTIMATION ACCEPTATION ("+best+")");
							}
							else
							{
								System.out.println(myAgent.getLocalName()+": NO PAINTER WAS FOUND");						
							}
							end = true;
						}

						// If no message has yet arrived, the behavior blocks itself
						else
						{
							block();
							end = false;
						}
					}

				}

				public boolean done ()
				{
					return end;
				}

			});


		}

	}
