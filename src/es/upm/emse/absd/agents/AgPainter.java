/*****************************************************************
 Agent offering the painting service: AgPainter.java
 *****************************************************************/
package es.upm.emse.absd.agents;

import es.upm.emse.absd.ontology.painters.Estimation;
import es.upm.emse.absd.ontology.painters.EstimationRequest;
import es.upm.emse.absd.ontology.painters.PaintServOntology;
import es.upm.emse.absd.ontology.painters.Proposes;
import jade.content.Concept;
import jade.content.ContentElement;
import jade.content.lang.Codec;
import jade.content.lang.Codec.CodecException;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/**
 * This agent has the following functionality:
 * <ul>
 * <li> It registers itself in the DF as PAINTER
 * <li> Waits for requests of its painting service
 * <li> If any estimation request arrives, it answers with a random value
 * <li> Finally, it waits for the client answer
 * </ul>
 *
 * @author Ricardo Imbert, UPM
 * @version $Date: 2008/04/09 13:35:18 $ $Revision: 1.0 $
 **/


public class AgPainter extends Agent {

    // Codec for the SL language used and instance of the ontology
    // PaintServOntology that we have created
    private final Codec codec = new SLCodec();
    private final Ontology ontology = PaintServOntology.getInstance();

    public final static String PAINTER = "Painter";


    protected void setup() {
        System.out.println(getLocalName() + ": has entered into the system");
        //      Register of the codec and the ontology to be used in the ContentManager
        getContentManager().registerLanguage(codec);
        getContentManager().registerOntology(ontology);
        try {
            // Creates its own description
            DFAgentDescription dfd = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            sd.setName(this.getName());
            sd.setType(PAINTER);
            dfd.addServices(sd);
            // Registers its description in the DF
            DFService.register(this, dfd);
            System.out.println(getLocalName() + ": registered in the DF");
            doWait(10000);
        } catch (FIPAException e) {
            e.printStackTrace();
        }

        //		BEHAVIOURS ****************************************************************

        // Adds a behavior to answer the estimation requests
        // Waits for a request and, when it arrives, answers with
        // the ESTIMATION and waits again.
        // If arrives a DECISION, it takes it (at this point, the painter would begin painting
        // if it is accepted...)

        addBehaviour(new CyclicBehaviour(this) {

            public void action() {
                // Waits for estimation requests
                ACLMessage msg = receive(
                    MessageTemplate.and(MessageTemplate.MatchLanguage(codec.getName()),
                        MessageTemplate.MatchOntology(ontology.getName())));
                if (msg != null) {

                    try {
                        ContentElement ce;
                        if (msg.getPerformative() == ACLMessage.CFP) {
                            // If an ESTIMATION request arrives (type Call for Proposal)
                            // it answers with the ESTIMATION

                            // The ContentManager transforms the message content (string)
                            // in java objects
                            ce = getContentManager().extractContent(msg);
                            // We expect an action inside the message
                            if (ce instanceof Action agAction) {
								Concept conc = agAction.getAction();
                                // If the action is EstimationRequest...
                                if (conc instanceof EstimationRequest) {
                                    System.out.println(myAgent.getLocalName()
                                        + ": received estimation request from "
                                        + (msg.getSender()).getLocalName());
                                    ACLMessage reply = msg.createReply();

                                    // A random estimation between 475 and 525 is calculated
                                    double sign = Math.random();
                                    double range = Math.random();

                                    if (sign < 0.5D)
                                        range *= -1D;
                                    int estim = (int) (500 + range * 25D);

                                    // An estimation is sent
                                    reply.setLanguage(codec.getName());
                                    reply.setOntology(ontology.getName());
                                    // A PROPOSE message is sent
                                    reply.setPerformative(ACLMessage.PROPOSE);
                                    Proposes prop = new Proposes();
                                    Estimation e = new Estimation();
                                    e.setAmount(estim);
                                    prop.setEstimation(e);
                                    try {
                                        // The ContentManager transforms the java objects in strings
                                        getContentManager().fillContent(reply, prop);
                                        myAgent.send(reply);
                                        System.out.println(
                                            myAgent.getLocalName() + ": answer sent -> " + estim);
                                    } catch (CodecException | OntologyException cex) {
                                        cex.printStackTrace();
                                    }
                                }
                            }
                        }
                        // If arrives an estimation proposal acceptation...
                        else if (msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) {
                            System.out.println(
                                myAgent.getLocalName() + ": received work acceptation from "
                                    + (msg.getSender()).getLocalName());
                        }
                        // If arrives an estimation rejection...
                        else if (msg.getPerformative() == ACLMessage.REJECT_PROPOSAL) {
                            System.out.println(
                                myAgent.getLocalName() + ": received work rejection from "
                                    + (msg.getSender()).getLocalName());
                        } else {
                            // If what is received is not understood
                            System.out.println(myAgent.getLocalName()
                                + ": Mamma mia!! non capisco il messaggio di "
                                + (msg.getSender()).getLocalName());
                            ACLMessage reply = msg.createReply();

                            //A NOT_UNDERSTOOD is sent
                            reply.setPerformative(ACLMessage.NOT_UNDERSTOOD);
                            myAgent.send(reply);
                            System.out.println(myAgent.getLocalName() + ": Perplexity sent");
                        }
                    } catch (CodecException | OntologyException e) {
                        e.printStackTrace();
                    }
                } else {
                    // If no message arrives
                    block();
                }

            }

        });

    }

}								
