package es.upm.emse.absd.ontology.painters;


import jade.content.*;
import jade.util.leap.*;
import jade.core.*;

/**
 * Protege name: Estimation
 *
 * @author ontology bean generator
 * @version 2010/04/12, 14:03:55
 */
public class Estimation implements Concept {

    /**
     * Protege name: amount
     */
    private int amount;

    public void setAmount(int value) {
        this.amount = value;
    }

    public int getAmount() {
        return this.amount;
    }

}
